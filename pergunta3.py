entrada = int(input("Valor de entrada: "))

def fibonacci(n: int):
    if (n < 2):
        return n
    else:
        return fibonacci(n - 1) + fibonacci(n - 2)

index = 0
while(True):
    if(fibonacci(index) == entrada):
        print("Pertence a Fibonacci")
        break
    if(fibonacci(index) > entrada):
        print("Não Pertence a Fibonacci")
        break
    index += 1