def ft_reverse(word: str):
	contrario = ''
	for i in range(1, len(word)+1):
		contrario += word[-i]
	return contrario

print(ft_reverse(str(input())))